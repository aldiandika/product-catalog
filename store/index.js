export const state = () => ({
  qty: 1,
})

export const mutations = {
  inc(state) {
    state.qty = state.qty + 1;
  },
  dec(state) {
    if (state.qty > 1) {
      state.qty = state.qty - 1;
    }
  },
}

export const getters = {
  getQty(state) {
    return state.qty;
  },
}